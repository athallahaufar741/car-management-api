"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Saveds", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      idcar: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Cars",
          key: "id",
        },
      },
      iduser: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "users",
          key: "id",
        },
      },
      creatby: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: "users",
          key: "email",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Saveds");
  },
};
